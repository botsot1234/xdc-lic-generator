package com.xdc.rest.service

import com.xdc.rest.dao.LicenseDao
import com.xdc.rest.entity.License
import com.xdc.rest.model.LicenseInfo
import com.xdc.rest.service.impl.LicenseServiceImpl
import spock.lang.Specification
import spock.lang.Subject

import javax.servlet.http.HttpServletRequest


/**
 * Created by Operator-PC on 8/12/2017.
 */
class LicenseServiceImplTest extends Specification {

    LicenseDao licenseDao = Mock()
    HttpServletRequest request = Mock()

    @Subject
    LicenseService sut

    def setup() {
        sut = new LicenseServiceImpl(licenseDao)
    }

    def itShouldAdd() {
        given:
        def license = new License(id: 1, genKey: "abc123")

        when:
        sut.add(license)

        then:
        1 * licenseDao.add(license) >> license
        license.getId() == 1
    }

    def itShouldGetOneWithKeyAsParams() {
        given:
        def key = "abc123"
        def license = new License(id: 1, genKey: key)

        when:
        sut.get(key)

        then:
        1 * licenseDao.get("abc123") >> license
        key == license.genKey
    }

    def itShouldUpdate() {
        given:
            def key = "abc123"
            def key1 = "abc1234"
            def license = new License(id: 1, genKey: key)
            def payload = new LicenseInfo(genKey: key1)

        when:
            sut.updateKey(key, payload)

        then:
            1 * licenseDao.get(key) >> license
            license != null
            1 * licenseDao.update(license) >> license
            license.genKey == key1
    }

    def itShouldGetAll() {
        given:
            def res = [new License(id: 1, genKey: "123"), new License(id: 2, genKey: "456")]

        when:
            sut.getAll(request)

        then:
            1 * licenseDao.getAll(request) >> res
            res.size() > 0
            res.get(0).genKey == "123"
    }

    def itShouldGetTotalCount() {
        given:
            def count = new Long(1000)

        when:
            sut.getTotal(request)

        then:
            1 * licenseDao.getCount(request) >> count
    }

}

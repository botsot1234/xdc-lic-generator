package com.xdc.rest.service

import com.xdc.rest.dao.CompanyDao
import com.xdc.rest.entity.Company
import com.xdc.rest.entity.License
import com.xdc.rest.model.CompanyInfo
import com.xdc.rest.model.CompanyInfoSave
import com.xdc.rest.model.LicenseInfo
import com.xdc.rest.service.impl.CompanyServiceImpl
import spock.lang.Specification
import spock.lang.Subject

import javax.servlet.http.HttpServletRequest

class CompanyServiceImplTest extends Specification {

    CompanyDao companyDao = Mock()

    @Subject
    CompanyService sut

    def setup() {
        sut = new CompanyServiceImpl(companyDao)
    }

    def "add"() {
        given:
        def company = new Company()

        when:
        sut.add(company)

        then:
        1 * companyDao.add(company) >> company
    }

    def "update"() {
        given:
        def company = new Company()

        when:
        sut.update(company)

        then:
        1 * companyDao.update(company) >> company
    }

    def "get"() {
        when:
        sut.get(1)
        then:
        1 * companyDao.get(1)
    }

    def "getAll"() {
        given:
        def res = [new Company()]

        when:
        sut.getAll(_ as HttpServletRequest)

        then:
        1 * companyDao.getAll(_ as HttpServletRequest) >> res
    }

    def "get total"() {
        when:
        sut.getTotal(_ as HttpServletRequest)

        then:
        1 * companyDao.getCount(_ as HttpServletRequest) >> 1L
    }

    def "generate license"() {
        given:
        def companyInfo = new CompanyInfo(
                apiPrefix: "api",
                name: "name"
        )
        def licenseCount = 1
        def license = new License()
        def resCompany = new Company(
                apiPrefix: "api",
                name: "name",
                licenses: [license]
        )

        when:
        sut.generateLicense(licenseCount, companyInfo)

        then:
        1 * companyDao.add(_ as Company) >> resCompany
        companyInfo.getName() == resCompany.getName()
        resCompany.licenses.size() == 1
    }

    def "persist comp lic"() {
        given:
        def license = new License()
        def licenseInfo = new LicenseInfo()
        def company = new Company(licenses: [license])
        def companyInfoSave = new CompanyInfoSave(company)
        companyInfoSave.setLicenseInfos([licenseInfo])


        when:
        sut.persistCompLic(companyInfoSave)

        then:
        1 * companyDao.add(company) >> company
    }
}

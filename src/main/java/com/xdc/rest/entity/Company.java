package com.xdc.rest.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the company database table.
 */
@Entity
@Table(name = "company")
@NamedQuery(name = "Company.findAll", query = "SELECT c FROM Company c")
public class Company implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String apiPrefix;
    private String name;
    private List<License> licenses;

    public Company() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @Column(name = "api_prefix", length = 50)
    public String getApiPrefix() {
        return this.apiPrefix;
    }

    public void setApiPrefix(String apiPrefix) {
        this.apiPrefix = apiPrefix;
    }


    @Column(length = 50)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    //bi-directional many-to-one association to License
    @OneToMany(mappedBy = "company", orphanRemoval = true,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    public List<License> getLicenses() {
        return this.licenses;
    }

    public void setLicenses(List<License> licenses) {
        this.licenses = licenses;
    }

}
package com.xdc.rest.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the user database table.
 */
@Entity
@Table(name = "user")
@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String password;
    private String username;
    private License license;

    public User() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @Column(length = 50)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Column(length = 50)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    //bi-directional many-to-one association to License
    @ManyToOne
    @JoinColumn(name = "license_id", nullable = false)
    public License getLicense() {
        return this.license;
    }

    public void setLicense(License license) {
        this.license = license;
    }

}
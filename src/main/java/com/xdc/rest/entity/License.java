package com.xdc.rest.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the license database table.
 */
@Entity
@Table(name = "license")
@NamedQuery(name = "License.findAll", query = "SELECT l FROM License l")
public class License implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private Integer clientEntityId;
    private String clientHost;
    private Date dateCreated;
    private Date dateExpired;
    private Timestamp dateModified;
    private String genKey;
    private Integer maxDeviceConnections;
    private String status;
    private String username;
    private Company company;
    private List<User> users;

    public License() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "client_entity_id")
    public Integer getClientEntityId() {
        return this.clientEntityId;
    }

    public void setClientEntityId(Integer clientEntityId) {
        this.clientEntityId = clientEntityId;
    }


    @Column(name = "client_host", length = 50)
    public String getClientHost() {
        return this.clientHost;
    }

    public void setClientHost(String clientHost) {
        this.clientHost = clientHost;
    }


    @Temporal(TemporalType.DATE)
    @Column(name = "date_created")
    public Date getDateCreated() {
        return this.dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "date_expired")
    public Date getDateExpired() {
        return this.dateExpired;
    }

    public void setDateExpired(Date dateExpired) {
        this.dateExpired = dateExpired;
    }

    @Column(name = "date_modified")
    public Timestamp getDateModified() {
        return this.dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    @Column(name = "gen_key")
    public String getGenKey() {
        return this.genKey;
    }

    public void setGenKey(String genKey) {
        this.genKey = genKey;
    }

    @Column(name = "max_device_connections")
    public Integer getMaxDeviceConnections() {
        return this.maxDeviceConnections;
    }

    public void setMaxDeviceConnections(Integer maxDeviceConnections) {
        this.maxDeviceConnections = maxDeviceConnections;
    }

    @Column(length = 1)
    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(length = 45)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //uni-directional many-to-one association to Company
    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    public Company getCompany() {
        return this.company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }


    //bi-directional many-to-one association to User
    @OneToMany(mappedBy = "license", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH},
            fetch = FetchType.EAGER)
    public List<User> getUsers() {
        return this.users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
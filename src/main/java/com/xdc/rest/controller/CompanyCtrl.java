package com.xdc.rest.controller;

import com.xdc.rest.entity.Company;
import com.xdc.rest.entity.License;
import com.xdc.rest.model.ApiResponse;
import com.xdc.rest.model.CompanyInfo;
import com.xdc.rest.model.CompanyInfoForm;
import com.xdc.rest.model.LicenseInfo;
import com.xdc.rest.security.RestCorsHttpServletResponse;
import com.xdc.rest.service.CompanyService;
import com.xdc.rest.utils.LicenseKeyGen;
import com.xdc.rest.utils.SysMsg;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Rest Controller for Company.
 */
@RestController
@RequestMapping("/rest/company")
public class CompanyCtrl {

    private static final Logger LOG = LoggerFactory.getLogger(CompanyCtrl.class);

    private CompanyService companyService;

    @Autowired
    public CompanyCtrl(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/list")
    public ApiResponse getAll(HttpServletRequest request, HttpServletResponse response) {
        RestCorsHttpServletResponse.setResponse(response);

        List<CompanyInfoForm> companyInfoForms = companyService.getAll(request)
                .stream().map(CompanyInfoForm::new)
                .collect(Collectors.toList());

        LOG.debug("Getting Company list: {}", companyInfoForms);

        Map<String, Object> payload = new HashedMap();
        payload.put("list", companyInfoForms);
        payload.put("count", companyService.getTotal(request));

        return new ApiResponse(SysMsg.SUCCESS_MESSAGE, payload);
    }

    @PostMapping("")
    public ApiResponse add(HttpServletResponse response, @RequestBody CompanyInfo companyInfo) {
        RestCorsHttpServletResponse.setResponse(response);

        Company company = new Company();
        company.setApiPrefix(companyInfo.getApiPrefix());
        company.setName(companyInfo.getName());

        List<License> licenses = new ArrayList<License>();
        for (LicenseInfo licenseInfo : companyInfo.getLicenseInfos()) {
            License license = new License();
            license.setCompany(company);
            license.setGenKey(LicenseKeyGen.genRandomString(100));
            license.setClientEntityId(licenseInfo.getClientEntityId());
            license.setClientHost(licenseInfo.getClientHost());
            license.setDateExpired(licenseInfo.getDateExpired());
            license.setMaxDeviceConnections(licenseInfo.getMaxDeviceConnections());
            license.setStatus(licenseInfo.getStatus());
            license.setUsername(licenseInfo.getUsername());
            licenses.add(license);
        }
        company.setLicenses(licenses);
        companyService.add(company);

        Map<String, Object> payload = new HashedMap();
        payload.put("status", true);

        return new ApiResponse(SysMsg.SUCCESS_MESSAGE, payload);
    }

    @PostMapping("/{licenseCount}")
    public ApiResponse generateLicense(@PathVariable Integer licenseCount,
                                       HttpServletResponse response,
                                       @RequestBody CompanyInfo companyInfo) {
        RestCorsHttpServletResponse.setResponse(response);

        Company company = companyService.generateLicense(licenseCount, companyInfo);

        Map<String, Object> payload = new HashedMap();
        payload.put("response", new CompanyInfoForm(company));

        return new ApiResponse(SysMsg.SUCCESS_MESSAGE, payload);
    }
}

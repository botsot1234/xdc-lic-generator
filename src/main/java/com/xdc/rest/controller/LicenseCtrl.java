package com.xdc.rest.controller;

import com.xdc.rest.entity.License;
import com.xdc.rest.model.ApiResponse;
import com.xdc.rest.model.LicenseInfo;
import com.xdc.rest.security.RestCorsHttpServletResponse;
import com.xdc.rest.service.LicenseService;
import com.xdc.rest.utils.SysMsg;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Operator-PC on 11/15/2016.
 */
@RestController
@RequestMapping("/rest/license")
public class LicenseCtrl {

    private static final Logger LOG = LoggerFactory.getLogger(LicenseCtrl.class);

    private LicenseService licenseService;

    @Autowired
    public LicenseCtrl(LicenseService licenseService) {
        this.licenseService = licenseService;
    }

    @GetMapping("/{genKey}")
    public ApiResponse get(@PathVariable String genKey, HttpServletResponse response) {
        RestCorsHttpServletResponse.setResponse(response);

        License license = licenseService.get(genKey);

        Map<String, Object> payload = new HashedMap();
        if (license != null) {
            payload.put("id", license.getId());
            payload.put("clientHost", license.getClientHost());
            payload.put("genKey", license.getGenKey());
            return new ApiResponse(SysMsg.LICENSE_FOUND_MESSAGE, payload);
        } else {
            return new ApiResponse(SysMsg.LICENSE_NOT_FOUND_MESSAGE, payload);
        }
    }

    @PutMapping("/{genKey}")
    public ApiResponse update(@PathVariable String genKey,
                              HttpServletResponse response, @RequestBody LicenseInfo licenseInfo) {
        RestCorsHttpServletResponse.setResponse(response);

        License license = licenseService.updateKey(genKey, licenseInfo);

        LOG.debug(" .. license info {}", licenseInfo);

        Map<String, Object> payload = new HashedMap();
        payload.put("status", true);
        payload.put("id", license.getId());
        return new ApiResponse(SysMsg.SUCCESS_MESSAGE, payload);
    }


    @GetMapping("/list")
    public ApiResponse getAll(HttpServletRequest request, HttpServletResponse response) {
        RestCorsHttpServletResponse.setResponse(response);

        List<LicenseInfo> licenseInfo = licenseService.getAll(request)
                .stream().map(LicenseInfo::new)
                .collect(Collectors.toList());

        Map<String, Object> payload = new HashedMap();
        payload.put("list", licenseInfo);
        payload.put("count", licenseService.getTotal(request));

        return new ApiResponse(SysMsg.SUCCESS_MESSAGE, payload);
    }
}

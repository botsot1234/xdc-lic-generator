package com.xdc.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings("deprecation")
@SpringBootApplication
@RestController
public class XDCLic extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(XDCLic.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(XDCLic.class);
    }

    @RequestMapping("/")
    public String home() {
        return "Home page";
    }
}

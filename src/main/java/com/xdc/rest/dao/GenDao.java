package com.xdc.rest.dao;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

interface GenDao<E> {
    int FIRST_INDEX = 0;
    /**
     * Use this field as count value to disregard count parameter and return all
     */
    int ALL = -1;

    E add(E entity) throws IllegalArgumentException;

    E get(Object key) throws IllegalArgumentException;

    E update(E entity) throws IllegalArgumentException;

    boolean remove(Object key) throws IllegalArgumentException;

    List<E> getAll(HttpServletRequest request) throws IllegalArgumentException;

    boolean exists(E entity) throws IllegalArgumentException;

    Long getCount(HttpServletRequest request);
}

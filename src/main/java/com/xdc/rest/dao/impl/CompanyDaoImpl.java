package com.xdc.rest.dao.impl;

import com.xdc.rest.dao.CompanyDao;
import com.xdc.rest.entity.Company;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Company Dao Implementation.
 */
@Repository
public class CompanyDaoImpl implements CompanyDao {

    @PersistenceContext
    private EntityManager entityManager;
    private Integer limit = 0;
    private Integer page = 1;
    private String genKey = "";

    @Override
    public Company add(Company entity) throws IllegalArgumentException {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public Company get(Object key) throws IllegalArgumentException {
        return entityManager.find(Company.class, key);
    }

    @Override
    public Company update(Company entity) throws IllegalArgumentException {
        return entityManager.merge(entity);
    }

    @Override
    public boolean remove(Object key) throws IllegalArgumentException {
        // TODO Auto-generated method stub
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Company> getAll(HttpServletRequest request) throws IllegalArgumentException {
        String qryCondition = setQueryCondition(request);
        Query query = entityManager.createQuery("SELECT c FROM Company AS c "
                + qryCondition + " ORDER BY c.id DESC");

        if (this.limit > 0) {
            query = query.setMaxResults(this.limit);
        }

        if (this.page > 0) {
            query = query.setFirstResult((this.page - 1) * this.limit);
        }

        if (this.genKey.length() > 0) {
            query = query.setParameter("genKey", this.genKey);
        }

        return query.getResultList();
    }

    @Override
    public boolean exists(Company entity) throws IllegalArgumentException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Long getCount(HttpServletRequest request) {
        String qryCondition = setQueryCondition(request);
        Query query = entityManager.createQuery("SELECT COUNT(*) FROM Company AS c "
                + qryCondition);

        if (this.genKey.length() > 0) {
            query = query.setParameter("genKey", this.genKey);
        }

        return (Long) query.getSingleResult();
    }

    private void setRequestParam(HttpServletRequest request) {

        if (request.getParameter("limit") != null) {
            this.limit = Integer.valueOf(request.getParameter("limit"));
        } else {
            this.limit = 0;
        }

        if (request.getParameter("page") != null) {
            this.page = Integer.valueOf(request.getParameter("page"));
        } else {
            this.page = 1;
        }

        if (request.getParameter("genKey") != null) {
            this.genKey = request.getParameter("genKey");
        } else {
            this.genKey = "";
        }
    }

    private String setQueryCondition(HttpServletRequest request) {

        setRequestParam(request);

        String qryCondition = "";
        String joinCondition = "";

        if (this.genKey.length() > 0) {
            joinCondition = " LEFT JOIN c.licenses AS cl ";

            if (!qryCondition.equals("")) {
                qryCondition += " AND ";
            }
            qryCondition += " cl.genKey = :genKey ";
        }

        if (!qryCondition.equals("")) {
            qryCondition = joinCondition + " where " + qryCondition;
        }

        return qryCondition;
    }


}

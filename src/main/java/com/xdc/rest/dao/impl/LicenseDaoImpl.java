package com.xdc.rest.dao.impl;

import com.xdc.rest.controller.LicenseCtrl;
import com.xdc.rest.dao.LicenseDao;
import com.xdc.rest.entity.License;
import com.xdc.rest.utils.SysMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * License Dao Implementation.
 */
@Repository
public class LicenseDaoImpl implements LicenseDao {

    private static final Logger LOG = LoggerFactory.getLogger(LicenseCtrl.class);

    @PersistenceContext
    private EntityManager entityManager;
    private Integer limit = 0;
    private Integer page = 1;

    @Override
    public License add(License entity) throws IllegalArgumentException {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public License get(Object key) throws IllegalArgumentException {
        return entityManager.find(License.class, key);
    }

    @Override
    public License update(License entity) throws IllegalArgumentException {
        return entityManager.merge(entity);
    }

    @Override
    public boolean remove(Object key) throws IllegalArgumentException {
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<License> getAll(HttpServletRequest request)
            throws IllegalArgumentException {

        Query query = entityManager.createQuery("SELECT l FROM License AS l ");

        if (this.limit > 0) {
            query = query.setMaxResults(this.limit);
        }

        if (this.page > 0) {
            query = query.setFirstResult((this.page - 1) * this.limit);
        }

        return query.getResultList();
    }

    @Override
    public boolean exists(License entity) throws IllegalArgumentException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Long getCount(HttpServletRequest request) {
        Query query = entityManager.createQuery("SELECT COUNT(*) FROM License AS l ");

        return (Long) query.getSingleResult();
    }


    @Override
    public License get(String genKey) throws EntityNotFoundException {
        Query query = entityManager.createQuery("SELECT l FROM License AS l "
                + " where l.genKey = :genKey and l.status = '1' ");
        query = query.setParameter("genKey", genKey);

        License license = (License) query.setMaxResults(1).getSingleResult();
        if (license == null) {
            LOG.error(SysMsg.LICENSE_NOT_FOUND_MESSAGE);
            throw new EntityNotFoundException("Not existing");
        }
        return license;
    }
}
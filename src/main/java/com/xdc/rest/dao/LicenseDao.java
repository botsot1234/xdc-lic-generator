package com.xdc.rest.dao;

import com.xdc.rest.entity.License;

import javax.persistence.EntityNotFoundException;

/**
 * Created by Operator-PC on 11/15/2016.
 */
public interface LicenseDao extends GenDao<License> {

    License get(String genKey) throws EntityNotFoundException ;

}

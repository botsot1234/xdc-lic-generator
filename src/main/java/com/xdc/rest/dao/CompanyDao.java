package com.xdc.rest.dao;

import com.xdc.rest.entity.Company;

/**
 * Company Dao Interface.
 */
public interface CompanyDao extends GenDao<Company> {

}

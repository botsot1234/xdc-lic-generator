package com.xdc.rest.security;

import javax.servlet.http.HttpServletResponse;

public final class RestCorsHttpServletResponse {
    public static HttpServletResponse setResponse(HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
        response.setHeader("Access-Control-Allow-Headers", "authorization, x-requested-with, content-type");
        response.setHeader("Access-Control-Max-Age", "1800"); //30 min

        return response;
    }

    private RestCorsHttpServletResponse() {

    }

}

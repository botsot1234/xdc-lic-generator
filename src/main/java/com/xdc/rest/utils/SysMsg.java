package com.xdc.rest.utils;

public final class SysMsg {
    private SysMsg() {

    }

    public static final String SUCCESS_MESSAGE = "Successful";
    public static final String NOT_EXISTING_MESSAGE = "Not Existing";
    public static final String LICENSE_FOUND_MESSAGE = "License Key Found";
    public static final String LICENSE_NOT_FOUND_MESSAGE = "License Key Not Found";
}

package com.xdc.rest.model;

import java.util.List;

public class CompanyInfo {
    private Integer companyId;
    private String apiPrefix;
    private String name;
    private List<LicenseInfo> licenseInfos;

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getApiPrefix() {
        return apiPrefix;
    }

    public void setApiPrefix(String apiPrefix) {
        this.apiPrefix = apiPrefix;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LicenseInfo> getLicenseInfos() {
        return licenseInfos;
    }

    public void setLicenseInfos(List<LicenseInfo> licenseInfos) {
        this.licenseInfos = licenseInfos;
    }
}

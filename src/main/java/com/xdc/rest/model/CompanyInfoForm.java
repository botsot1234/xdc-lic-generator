package com.xdc.rest.model;

import com.google.common.collect.Lists;
import com.xdc.rest.entity.Company;
import com.xdc.rest.entity.License;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;
import java.util.Optional;

public class CompanyInfoForm extends AbstractWebForm<Company> {

    public CompanyInfoForm(Company target) {
        super(target);
    }

    public CompanyInfoForm() {
        super(new Company());
    }

    public String getName() {
        return target().getName();
    }

    public Integer getId() {
        return target().getId();
    }

    public String getApiPrefix() {
        return target().getApiPrefix();
    }

    public List<LicenseInfo> getLicenses() {
        List<License> licenses = Optional.ofNullable(target().getLicenses()).orElse(Lists.newArrayList());
        return WebDtoUtils.wrapDto(licenses, LicenseInfo.class);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", target().getName())
                .append("apiPrefix", target().getApiPrefix())
                .toString();
    }
}

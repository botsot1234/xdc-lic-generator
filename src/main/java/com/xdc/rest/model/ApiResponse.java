package com.xdc.rest.model;

/**
 * Created by Operator-PC on 10/15/2016.
 */
public class ApiResponse {
    private String message;
    private Object payload;

    public ApiResponse(String message) {
        this.message = message;
    }

    public ApiResponse(String message, Object payload) {
        this.message = message;
        this.payload = payload;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}

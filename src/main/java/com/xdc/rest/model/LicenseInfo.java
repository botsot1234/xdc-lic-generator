package com.xdc.rest.model;

import com.xdc.rest.entity.Company;
import com.xdc.rest.entity.License;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;

public class LicenseInfo extends AbstractWebForm<License> {

    public LicenseInfo(License target) {
        super(target);
    }

    public LicenseInfo() {
        super(new License());
    }

    public Integer getId() {
        return target().getId();
    }

    public String getGenKey() {
        return target().getGenKey();
    }

    public void setGenKey(String genKey) {
        target().setGenKey(genKey);
    }

    public void setCompany(Company company) {
        target().setCompany(company);
    }

    public Integer getClientEntityId() {
        return target().getClientEntityId();
    }

    public String getClientHost() {
        return target().getClientHost();
    }

    public Integer getMaxDeviceConnections() {
        return target().getMaxDeviceConnections();
    }

    public String getStatus() {
        return target().getStatus();
    }

    public Date getDateExpired() {
        return target().getDateExpired();
    }

    public String getUsername() {
        return target().getUsername();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", target().getId())
                .append("key", target().getGenKey())
                .toString();
    }
}

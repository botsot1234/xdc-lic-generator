package com.xdc.rest.model;

import com.xdc.rest.entity.User;

public class UserInfo extends AbstractWebForm<User> {

    public UserInfo(User target) {
        super(target);
    }

    public UserInfo() {
        super(new User());
    }

    public Integer getId() {
        return target().getId();
    }

    public String getUsername() {
        return target().getUsername();
    }

    public String getPassword() {
        return target().getPassword();
    }
}

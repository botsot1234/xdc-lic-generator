package com.xdc.rest.model;

import com.google.common.collect.Lists;
import com.xdc.rest.entity.Company;

import java.util.List;

/**
 * Created by rfetalvero on 3/11/17.
 */
public class CompanyInfoSave extends AbstractWebForm<Company> {

    private List<LicenseInfo> licenseInfos = Lists.newArrayList();

    public CompanyInfoSave(Company target) {
        super(target);
    }

    public CompanyInfoSave() {
        super(new Company());
    }

    public String getName() {
        return target().getName();
    }

    public void setName(String name) {
        target().setName(name);
    }

    public Integer getId() {
        return target().getId();
    }

    public void setId(Integer id) {
        target().setId(id);
    }

    public String getApiPrefix() {
        return target().getApiPrefix();
    }

    public void setApiPrefix(String apiPrefix) {
        target().setApiPrefix(apiPrefix);
    }

    public List<LicenseInfo> getLicenseInfos() {
        return licenseInfos;
    }

    public void setLicenseInfos(List<LicenseInfo> licenseInfos) {
        this.licenseInfos = licenseInfos;
    }
}

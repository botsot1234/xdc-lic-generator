package com.xdc.rest.model;

import java.io.Serializable;

public interface WebDto<TARGET> extends Serializable {

    TARGET target();

}

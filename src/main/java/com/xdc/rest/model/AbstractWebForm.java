package com.xdc.rest.model;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public abstract class AbstractWebForm<T> implements WebForm<T> {
    private final T target;

    protected AbstractWebForm(T target) {
        Validate.notNull(target, "Web form target model cannot be null.");
        this.target = target;
    }

    @Override
    public T target() {
        return target;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
                .append("target", target)
                .toString();
    }
}

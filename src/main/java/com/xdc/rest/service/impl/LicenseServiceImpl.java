package com.xdc.rest.service.impl;

import com.xdc.rest.dao.LicenseDao;
import com.xdc.rest.entity.License;
import com.xdc.rest.model.LicenseInfo;
import com.xdc.rest.service.LicenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by rfetalvero on 3/11/17.
 */
@Service
@Transactional
public class LicenseServiceImpl implements LicenseService {

    private LicenseDao licenseDao;

    @Autowired
    public LicenseServiceImpl(LicenseDao licenseDao) {
        this.licenseDao = licenseDao;
    }

    public License get(String genkey) {
        return licenseDao.get(genkey);
    }

    public License add(License license) {
        return licenseDao.add(license);
    }


    public List<License> getAll(HttpServletRequest request) {
        return licenseDao.getAll(request);
    }

    public Long getTotal(HttpServletRequest request) {
        return licenseDao.getCount(request);
    }

    public License updateKey(String genKey, LicenseInfo licenseInfo) {

        License license = licenseDao.get(genKey);
        license.setGenKey(licenseInfo.getGenKey());
        license.setClientEntityId(licenseInfo.getClientEntityId());
        license.setClientHost(licenseInfo.getClientHost());
        license.setDateExpired(licenseInfo.getDateExpired());
        license.setMaxDeviceConnections(licenseInfo.getMaxDeviceConnections());
        license.setStatus(licenseInfo.getStatus());
        license.setUsername(licenseInfo.getUsername());
        return licenseDao.update(license);
    }
}

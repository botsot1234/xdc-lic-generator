package com.xdc.rest.service.impl;

import com.xdc.rest.dao.CompanyDao;
import com.xdc.rest.entity.Company;
import com.xdc.rest.entity.License;
import com.xdc.rest.model.CompanyInfo;
import com.xdc.rest.model.CompanyInfoSave;
import com.xdc.rest.service.CompanyService;
import com.xdc.rest.utils.LicenseKeyGen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by rfetalvero on 3/11/17.
 */
@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {

    private CompanyDao companyDao;

    @Autowired
    public CompanyServiceImpl(CompanyDao companyDao) {
        this.companyDao = companyDao;
    }

    public Company add(Company company) {
        return companyDao.add(company);
    }

    public Company get(int id) {
        return companyDao.get(id);
    }

    public Company update(Company company) {
        return companyDao.update(company);
    }

    public List<Company> getAll(HttpServletRequest request) {
        return companyDao.getAll(request);
    }

    public Long getTotal(HttpServletRequest request) {
        return companyDao.getCount(request);
    }

    public Company generateLicense(Integer licenseCount, CompanyInfo companyInfo) {

        Company company = new Company();
        company.setApiPrefix(companyInfo.getApiPrefix());
        company.setName(companyInfo.getName());

        List<License> licenses = new ArrayList<>();
        for (int i = 0; i < licenseCount; i++) {
            License license = new License();
            license.setCompany(company);
            license.setGenKey(LicenseKeyGen.genRandomString(100));
            license.setDateCreated(new Date());
            license.setDateModified(null);
            license.setStatus("1");
            license.setClientHost(companyInfo.getApiPrefix());
            licenses.add(license);
        }

        company.setLicenses(licenses);

        return companyDao.add(company);
    }

    @Override
    public Company persistCompLic(CompanyInfoSave companyInfoSave) {
        final Company company = companyInfoSave.target();
        List<License> licenses = new ArrayList<>();
        companyInfoSave.getLicenseInfos().forEach(licenseInfo -> {
            licenseInfo.setCompany(company);
            licenses.add(licenseInfo.target());
        });
        company.setLicenses(licenses);
        return companyDao.add(company);
    }
}

package com.xdc.rest.service;

import com.xdc.rest.entity.License;
import com.xdc.rest.model.LicenseInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface LicenseService {

    License get(String genkey);

    License add(License license);

    List<License> getAll(HttpServletRequest request);

    Long getTotal(HttpServletRequest request);

    License updateKey(String genKey, LicenseInfo licenseInfo);

}
package com.xdc.rest.service;

import com.xdc.rest.entity.Company;
import com.xdc.rest.model.CompanyInfo;
import com.xdc.rest.model.CompanyInfoSave;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface CompanyService {

    Company add(Company company);

    Company get(int id);

    Company update(Company company);

    List<Company> getAll(HttpServletRequest request);

    Long getTotal(HttpServletRequest request);

    Company generateLicense(Integer licenseCount, CompanyInfo companyInfo);

    Company persistCompLic(CompanyInfoSave companyInfoSave);
}

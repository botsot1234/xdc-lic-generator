# XDCorp License Service #

This Restful service is implemented in Spring Boot

### Prerequisites ###
Maven 3 above
Java 8
MySQL 5.6 above
IDE (it depends in what you are comfortable as long it is Maven supported) PS Notepad will do

### Installation ###
1. Create database xdcorp_abs_lic
2. Build the project
3. cd /path/to/project
4. ```mvn clean install```
5. ```mvn clean install -Dcheckstyle.skip=true``` to skip checkstyle checking,
not recommended in development. Makes you a hypocrite in not following a certain
convention.
6. ```mvn clean install -DskipTests``` to skip Unit Testing invocation, not recommended also

### Running on Development ###
1. cd /path/to/project
2. ```mvn spring-boot:run -Dspring.profiles.active=dev``` or ```mvn spring-boot:run```
  because default profile/config is set to development
3. check if app is running via browser paste this link

check serverPort value in resources/application.properties

```
localhost:{serverPort}/xdcorp-abs-lic
```


### Deployment ###
Execute the jar and detach
```java -jar target/xdc-abs-lic.jar -Dspring.profiles.active=production &```


### What is this repository for? ###

This is for the back end service of license key for the Android Platform.

### How do I get set up? ###

Download the source code mvn clean install and package.

### Contribution guidelines ###

Just fork the repo or create a pull request.

### Who do I talk to? ###

##Read the documentations regarding the technologies used here.##
1. Spring Boot - https://projects.spring.io/spring-boot/
2. MySQL - https://www.mysql.com/
3. Maven 3 - https://maven.apache.org/

### Last Resort ###

Talk to Rod (he will point you to previous recommendation ::evil_laugh)
